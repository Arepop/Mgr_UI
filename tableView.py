import os
from PyQt5 import uic, QtWidgets, QtCore, QtGui, Qt
import popup


class MyStyle(QtWidgets.QProxyStyle):

    def drawPrimitive(self, element, option, painter, widget=None):
        """
        Draw a line across the entire row rather than just the column
        we're hovering over.  This may not always work depending on global
        style - for instance I think it won't work on OSX.
        """
        if element == self.PE_IndicatorItemViewItemDrop and not option.rect.isNull():
            option_new = QtWidgets.QStyleOption(option)
            option_new.rect.setLeft(0)
            if widget:
                option_new.rect.setRight(widget.width())
            option = option_new
        super().drawPrimitive(element, option, painter, widget)


class MyModel(QtGui.QStandardItemModel):
    def dropMimeData(self, data, action, row, col, parent):
        """Always move the entire row, and don't allow column "shifting"
        """
        return super().dropMimeData(data, action, row, 0, parent)


class MyTableView(QtWidgets.QTableView):

    def __init__(self, parent):
        super().__init__(parent)
        self.setSelectionBehavior(self.SelectRows)
        self.setSelectionMode(self.SingleSelection)
        self.setDragDropOverwriteMode(False)
        self.setShowGrid(True)
        self.setDragDropMode(self.DropOnly)
        self.model = MyModel()
        self.setModel(self.model)
        self.style = MyStyle()
        self.setStyle(self.style)
        self.origin = 1

    def create_action(self):
        dialog = popup.ActionTime()
        dialog.show()
        dialog.save_act_btn.clicked.connect(
            lambda: self.add_action(dialog.lineEdit.text()))
        dialog.cancel_act_btn.clicked.connect(lambda: dialog.close())

    def dropEvent(self, event):
        if event.source().origin:
            return super().dropEvent(event)
        else:
            data = event.mimeData()
            source_item = QtGui.QStandardItemModel()
            source_item.dropMimeData(
                data, QtCore.Qt.CopyAction, 0, 0, QtCore.QModelIndex())
            data = source_item.item(0, 0).data()
            if data is not None:
                self.append_data(source_item.item(0, 0).text(), data)

    def enterEvent(self, event):
        self.setDragDropMode(self.InternalMove)
        return super(MyTableView, self).enterEvent(event)

    def mouseDoubleClickEvent(self, event):
        col = self.find_col()
        print(col)
        self.create_action()

    def find_col(self):
        xx = self.selectionModel()
        rowr = xx.selectedRows()[0].row()
        for col in range(self.model.columnCount()):
            col_data = self.model.item(rowr, col)
            if col_data != None:
                if col_data.data() == 1:
                    return col

    def move_column_to(self, place):
        pass

    def append_data(self, text, data):
        full = self.model.columnCount()
        if not full:
            data = [text] + data
        else:
            data = [text] + ['']*(full-1) + data
        data = [QtGui.QStandardItem(idx) for idx in data]
        data[0].setEditable(False)
        data[0].setDropEnabled(False)

        for item in data[1:]:
            if item.text() != '':
                item.setText(None)
                # brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
                # brush.setStyle(QtCore.Qt.SolidPattern)
                # item.setBackground(brush)
                item.setData(1)
            item.setEditable(False)
            item.setDropEnabled(False)
        self.model.appendRow(data)
