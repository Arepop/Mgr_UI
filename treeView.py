from textwrap import wrap
from PyQt5 import uic, QtWidgets, QtCore, QtGui


class StandardItemModel(QtGui.QStandardItemModel):

    def dropMimeData(self, data, action, row, col, parent):
        """Always move the entire row, and don't allow column "shifting"
        """
        return super().dropMimeData(data, action, row, col, parent)

    def populate(self):
        for row in range(0, 3):
            parentItem = QtGui.QStandardItem()
            parentItem.setText(f'A{row}')
            self.appendRow(parentItem)
            for col in range(0, 10):
                item = QtGui.QStandardItem()
                item.setText(f'A{col}')
                item.setData([f'{row+col}'])
                item.setEditable(False)
                item.setDropEnabled(False)
                parentItem.appendRow(item)


class MyTreeView(QtWidgets.QTreeView):
    def __init__(self, parent, other):
        super().__init__(parent)
        self.model = StandardItemModel()
        self.model.populate()
        self.setModel(self.model)
        self.setDragEnabled(True)
        self.setDragDropOverwriteMode(False)
        self.setDragDropMode(self.DragOnly)
        self.test = other
        self.origin = 0

    def enterEvent(self, event):
        self.test.setDragDropMode(self.DropOnly)
        return super(MyTreeView, self).enterEvent(event)
