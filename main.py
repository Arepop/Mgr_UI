import os
import sys
from PyQt5 import uic, QtWidgets, QtCore, QtGui
import popup
import treeView
import tableView


class UiWindow(QtWidgets.QMainWindow, QtWidgets.QWidget):
    """Main window
    """

    def __init__(self):
        super(UiWindow, self).__init__()
        dir_path = os.path.dirname(os.path.realpath(__file__))
        uic.loadUi(dir_path + "\\main.ui", self)

        # Windows options
        self.setWindowTitle('UI ver. alpha')
        self.data = ['1', '2', '3']

        # TableViewp
        tv = tableView.MyTableView(self)
        self.horizontalLayout_2.addWidget(tv)
        self.init_button.clicked.connect(lambda: tv.append_data(self.data))

        # TreeView
        tree = treeView.MyTreeView(self, tv)
        self.horizontalLayout.addWidget(tree)

        # Popup menu
        self.popMenu = QtWidgets.QMenu(self)
        self.test0 = QtWidgets.QAction('test0', self)
        self.popMenu.addAction(self.test0)
        self.test0.triggered.connect(self.open_ini)
        self.popMenu.addAction(QtWidgets.QAction('test1', self))
        self.popMenu.addSeparator()
        self.popMenu.addAction(QtWidgets.QAction('test2', self))

        # Load data
        self.actions = dict()
        with open('test.ini', 'r') as act:
            for line in act.readlines():
                line = line.replace('\n', '')
                print(line.split(':')[
                    1].split(','))
                self.actions[line.split(':')[0]] = line.split(':')[
                    1].split(',')
        print(self.actions)

    def on_context_menu(self, point):
        self.popMenu.exec_(self.action_1.mapToGlobal(point))

    def open_ini(self):
        dialog = popup.PopupWindow()
        dialog.show()
        dialog.save_action_button.clicked.connect(
            lambda: self.action_1.setText(dialog.lineEdit.text()))
        dialog.cancel_act_btn.clicked.connect(lambda: dialog.close())

    def create_action(self):
        dialog = popup.PopupWindow2()
        dialog.show()
        dialog.add_act_btn.clicked.connect(
            lambda: self.add_action(dialog.lineEdit.text()))
        dialog.cancel_act_btn.clicked.connect(lambda: dialog.close())

    def add_action(self, name):
        self.new_action = QtWidgets.QPushButton()
        self.new_action.setText(name)
        self.actionLayout.addWidget(
            self.new_action, self.actionLayout.rowCount()+1, 0)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    GUI = UiWindow()
    GUI.show()
    sys.exit(app.exec_())
