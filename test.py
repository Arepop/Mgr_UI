import sys
from PyQt5 import QtGui, QtCore, QtWidgets


class StandardItemModel(QtGui.QStandardItemModel):
    def __init__(self, parent=None):
        super(StandardItemModel, self).__init__(parent)

    def populate(self):
        for row in range(0, 10):
            parentItem = self.invisibleRootItem()
            for col in range(0, 4):
                item = QtGui.QStandardItem("item (%s, %s)" % (row, col))
                parentItem.appendRow(item)
                parentItem = item


class MainForm(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainForm, self).__init__(parent)

        self.model = StandardItemModel()
        self.model.populate()

        self.view = QtWidgets.QTreeView()
        self.view.setModel(self.model)
        self.view.setHeaderHidden(True)
        self.view.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

        self.setCentralWidget(self.view)


def main():
    app = QtWidgets.QApplication(sys.argv)
    form = MainForm()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
