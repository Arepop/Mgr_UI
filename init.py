import pickle


def define_action():
    action_1 = {'Name': 'name', 'A1': 1, 'a2': 6}
    action_2 = {'Name': 'name2', 'A2': 2, 'a6': 4}
    action_3 = {'Name': 'name3', 'A3': 3, 'a7': 2}
    action_4 = {'Name': 'name4', 'A4': 4, 'a8': 3}
    actions = [action_1, action_2, action_3, action_4]
    with open('test2.ini', 'wb') as ini2:
        pickle.dump(actions, ini2)


def load_ini_file(file):
    pickle.load(file)


if __name__ == '__main__':
    with open('test2.ini', 'rb') as ini:
        ars = pickle.load(ini)
    print(ars[1])
