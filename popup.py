import os
from PyQt5 import uic, QtWidgets, QtCore, QtGui


class PopupWindow(QtWidgets.QDialog):
    def __init__(self):
        super(PopupWindow, self).__init__()

        dir_path = os.path.dirname(os.path.realpath(__file__))
        uic.loadUi(dir_path + "\\popup2.ui", self)
        self.setWindowTitle('UTIN window')


class PopupWindow2(QtWidgets.QDialog):
    def __init__(self):
        super(PopupWindow2, self).__init__()

        dir_path = os.path.dirname(os.path.realpath(__file__))
        uic.loadUi(dir_path + "\\popup.ui", self)
        self.setWindowTitle('UTIN window c')


class ActionTime(QtWidgets.QDialog):
    def __init__(self):
        super(ActionTime, self).__init__()

        dir_path = os.path.dirname(os.path.realpath(__file__))
        uic.loadUi(dir_path + "\\actionstst.ui", self)
        self.setWindowTitle('Set Time')
